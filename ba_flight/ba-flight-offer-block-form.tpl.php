  <script type="text/javascript" src="https://www.google.com/jsapi"></script>
   <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
   </script>  
<div class="flightinfo_panel">
<form name="ba_flight_offer_block_form">
<input type="hidden" id="journey_type" name="journey_type" value="roundTrip">
<input type="hidden" id="range" name="range" value="monthLow">
<input type="hidden" id="cabin" name="cabin" value="economy">
Departure city: <input type="text" id="departure_city" name="departure_city" size="3" maxlength="3">
<br />
Arrival City:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="text" id="arrival_city" name="arrival_city" size="3" maxlength="3">
<br />
<input type="button" id="ba_flight_offer_block_submit_button" name="ba_flight_offer_block_submit_button" value="Search">
</form>
<div id="ba_flight_message">
</div>

<div id="chart_div"></div>
</div>
