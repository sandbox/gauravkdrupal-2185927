Drupal.behaviors.ba_flight = function(context) {

$("#ba_flight_offer_block_submit_button").click(function(){

departure_city 	= $('#departure_city').val();
arrival_city 	= $('#arrival_city').val();
journey_type 	= $('#journey_type').val();
range 			= $('#range').val();
cabin 			= $('#cabin').val();

$("#ba_flight_message").html('Loading...');

$.post(Drupal.settings.basePath + "ba_flight_offer_modal", {"departure_city":departure_city, "arrival_city":arrival_city, "journey_type":journey_type, "range":range, "cabin":cabin}, ba_flight_offer_callback);
});
}


var ba_flight_offer_callback = function(response) {
	var result = response;
	
	if(IsJson(result)){
		$("#ba_flight_message").html('');
		drawChart(result);
	}
	else{
		$("#ba_flight_message").html(result);
    }
}

      function drawChart(result) {
		var jsonobj = eval ("(" + result + ")");
        
        var data = google.visualization.arrayToDataTable([
          ['Month', 'Price'],
          [jsonobj[0],  jsonobj[1]],
          [jsonobj[2],  jsonobj[3]],
          [jsonobj[4],  jsonobj[5]]
        ]);

        var options = {
          vAxis: {title: 'Month',  titleTextStyle: {color: 'red'}},
          hAxis: {title: "Price",  titleTextStyle: {color: 'red'}},
          width:299, height:200
        };

        var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
			chart.draw(data, options);
      }

function IsJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

