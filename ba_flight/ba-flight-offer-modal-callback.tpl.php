<?php
	$response_code 	= $content->code;
	$status_message = $content->status_message;
	
	$content_data = $content->data;
	$raw_content = json_decode($content_data, true);

	if($response_code == '200'){

	if($range == 'monthLow'){
	
	$itinerary0 = $raw_content["PricedItinerariesResponse"]["PricedItinerary"][0];
	$arr[] = $itinerary0["TravelMonth"];
	$arr[] = $itinerary0["Price"]["Amount"]["Amount"];
	
	$itinerary3 = $raw_content["PricedItinerariesResponse"]["PricedItinerary"][1];
	$arr[] = $itinerary3["TravelMonth"];
	$arr[] = $itinerary3["Price"]["Amount"]["Amount"];
	
	$itinerary6 = $raw_content["PricedItinerariesResponse"]["PricedItinerary"][2];
	$arr[] = $itinerary6["TravelMonth"];
	$arr[] = $itinerary6["Price"]["Amount"]["Amount"];
	
	print json_encode($arr);
	} 
	
}
else{
?>
  <div class="searchResult">
	<span class=\"ba_flight_error_message\">
  <?php
	if($response_code == 400){
		print "Please enter valid city.";
	}
	elseif($response_code == 404)
	{
		print "No offer found for the arrival city.";
	}
	else{
		print "An Error Has Occurred";
		print $raw_content["Errors"]["Error"] . "<br />";
		print 'Error Code: ' . $response_code . ";  ";
		print $status_message;
	}
	?>	
	</span>
	</div>	
<?php
} 




